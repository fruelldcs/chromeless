const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const DedupePlugin = require('webpack/lib/optimize/DedupePlugin');
const UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const CompressionPlugin = require('compression-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const TS_VERSION = require('typescript').version;
const ENV = (process.env.ENV = process.env.NODE_ENV = 'production');
// the minimum IE version this project will run on, for no IE support, set IE to 12 or above
const IE = 11;

const commonConfig = require('./webpack.common.js');
const helpers = require('./helpers');
const buildPath = helpers.root('build', 'production');

const polyfills = require('./polyfills/polyfills');

let iePolyfills = [];

if (IE === 11) {
  iePolyfills = [...polyfills.ie11];
}
if (IE < 11) {
  iePolyfills = [...polyfills.ie10];
}
if (IE < 10) {
  iePolyfills = [...polyfills.ie9];
}

module.exports = webpackMerge(commonConfig, {
  entry: {
    polyfills: iePolyfills,
    worker: './config/polyfills/serviceWorker',
    main: './src/main.aot.ts'
  },

  devtool: 'source-map',

  output: {
    path: buildPath,
    filename: '[name].[chunkhash].bundle.js',
    sourceMapFilename: '[name].[chunkhash].map',
    chunkFilename: '[id].[chunkhash].chunk.js'
  },

  module: {
    loaders: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              compilerOptions: {
                // thanks angular https://github.com/angular/angular/issues/17131
                noUnusedParameters: false
              }
            }
          },
          'angular-router-loader?aot=true&genDir=foo/bar',
          'angular2-template-loader'
        ],
        exclude: [/\.(spec|e2e-spec)\.ts$/]
      }
    ]
  },

  plugins: [
    new WebpackMd5Hash(),
    new OfflinePlugin({
      updateStrategy: 'changed',
      ServiceWorker: {
        events: true,
        cacheName: 'dcs-angular-starter'
      },
      AppCache: {
        events: false
      }
    }),

    new UglifyJsPlugin({
      comments: false,
      sourceMap: true
    }),

    new CompressionPlugin({
      regExp: /\.css$|\.html$|\.js$|\.map$/,
      threshold: 2 * 1024
    }),

    new DefinePlugin({
      ENV: JSON.stringify(ENV),
      'process.env.NODE_ENV': JSON.stringify(ENV),
      IE: IE,
      TS_VERSION: JSON.stringify(TS_VERSION)
    }),

    new webpack.LoaderOptionsPlugin({
      debug: false
    }),

    new HtmlWebpackPlugin({
      template: 'src/index.html',
      chunksSortMode: helpers.orderByList(['common', 'styles', 'polyfills', 'vendor', 'main', 'worker'])
    }),

    new CircularDependencyPlugin({
      // exclude detection of files based on a RegExp
      exclude: /a\.js|node_modules/,
      // add errors to webpack instead of warnings
      failOnError: true
    })
  ]
});
