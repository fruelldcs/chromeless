if ( ('serviceWorker' in window.navigator) || ('applicationCache' in window) ) {
  const runtime = require('offline-plugin/runtime');
  const TIMEOUT = 120000;

  runtime.install({
    onUpdating: function () {
      console.log('SW Event:', 'onUpdating');
    },
    onUpdateReady: function () {
      console.log('SW Event:', 'onUpdateReady');
      // Tells to new SW to take control immediately
      runtime.applyUpdate();
    },
    onUpdated: function () {
      console.log('SW Event:', 'onUpdated');
      // Reload the webpage to load into the new version
      window.location.reload();
    },
    onUpdateFailed: function () {
      console.log('SW Event:', 'onUpdateFailed');
    }
  });

  if ('serviceWorker' in window.navigator) {
    navigator.serviceWorker.getRegistration().then(function(registration) {
      setInterval(function () {
        console.log('trying to update service worker');
        if (registration) {
          registration.update();
        }
      }, TIMEOUT);
    });

    // for testing the prod environment, manually reset the service worker to bust all caches
    window.resetWorker = function() {
      navigator.serviceWorker.getRegistrations().then(function(registrations) {
        registrations.forEach(function(registration) {
          registration.unregister();
        });
      });
    };

  } else if ('applicationCache' in window) {
    setInterval(function() {
      window.applicationCache.update();
    }, TIMEOUT);
  }

}
