
let polyfills = [
  // see https://github.com/angular/angular/issues/13609
  // angular 2.4.1 breaks 'core-js/es7/reflect'
  // 'core-js/es7/reflect',
  'reflect-metadata',
  'core-js/es6/symbol',
  'core-js/es6/object',
  'core-js/es6/function',
  'core-js/es6/parse-int',
  'core-js/es6/parse-float',
  'core-js/es6/number',
  'core-js/es6/math',
  'core-js/es6/string',
  'core-js/es6/date',
  'core-js/es6/array',
  'core-js/es6/regexp',
  'core-js/es6/map',
  'core-js/es6/set',
  'core-js/es6/weak-map',
  'core-js/es6/weak-set',
  'core-js/es6/typed',
  'core-js/es6/reflect',
  'core-js/es6/promise',

  'core-js/es7/object',
  'core-js/es7/array',

  'zone.js/dist/zone'
];

let ie11 = [
  // IE10 & IE11 do not completely implement classList
  // needed for some edge cases with ngClass (like classes on SVG elements)
  'classlist-polyfill',
];

let ie10 = [
  'intl',
  // // add more languages if necessary for project
  'intl/locale-data/jsonp/de-DE',
  'intl/locale-data/jsonp/en-US',

  // if you make use of advanced console commands: console.group (like redux-logger), console.time etc.
  // needed for IE < 11
  'console-polyfill',
];

let ie9 = [
  // If we need to support IE9
  // needed for HTTP with binary data
  'blob-polyfill',
  'typedarray',
  // found no npm installable package for this one
  './config/polyfills/polyfill-formdata'
];

exports.imports = polyfills;
exports.ie11 = ie11;
exports.ie10 = ie10;
exports.ie9 = ie9;
