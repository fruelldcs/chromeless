const webpack = require('webpack');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ForkCheckerPlugin } = require('awesome-typescript-loader');
const { CheckerPlugin } = require('awesome-typescript-loader');

const helpers = require('./helpers');

const polyfills = require('./polyfills/polyfills').imports;

let cssLoaderSetting = 'css-loader';
if (process.env.ENV === 'production') {
  cssLoaderSetting += '?minimize';
}

module.exports = {
  entry: {
    polyfills: polyfills,
    main: './src/main.ts',
    styles: './src/app/app.scss'
  },

  resolve: {
    extensions: ['.ts', '.js', '.json'],
    unsafeCache: true,
    modules: [helpers.root('src'), helpers.root('node_modules')]
  },

  performance: {
    hints: false
  },

  module: {
    exprContextCritical: false,

    loaders: [
      {
        test: /\.js$/,
        use: ['source-map-loader'],
        enforce: 'pre',
        exclude: [
          helpers.root('node_modules/@angular/compiler'),
          // if dev setup via rush, the @angular/compiler lies there
          helpers.root('../common/temp/node_modules/@angular/compiler')
        ]
      },

      {
        test: /\.json$/,
        loader: 'json-loader'
      },

      {
        test: /\.css$/,
        loaders: ['to-string-loader', 'css-loader']
      },

      {
        test: /\.html$/,
        loader: 'raw-loader',
        exclude: [helpers.root('src/index.html')]
      },

      {
        test: /\.scss$/,
        loaders: ['style-loader', cssLoaderSetting, 'sass-loader', 'postcss-loader']
      },

      {
        test: /\.less$/,
        loaders: ['style-loader', cssLoaderSetting, 'less-loader', 'postcss-loader']
      },

      {
        test: /\.(png|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader'
      }
    ]
  },

  plugins: [
    new CheckerPlugin(),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|fr/),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      chunks: ['main'],
      minChunks: module => /node_modules/.test(module.resource)
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common'
    }),

    new CopyWebpackPlugin([
      {
        from: 'src/assets',
        to: 'assets'
      },
      {
        from: 'src/manifest.json',
        to: ''
      }
    ]),

    new HtmlWebpackPlugin({
      template: 'src/index.html',
      chunksSortMode: helpers.orderByList(['common', 'styles', 'polyfills', 'vendor', 'main'])
    })
  ]
};
