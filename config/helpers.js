var path = require('path');

// Helper functions
var ROOT = path.resolve(__dirname, '..');

function hasProcessFlag(flag) {
  return process.argv.join('').indexOf(flag) > -1;
}

function isWebpackDevServer() {
  return process.argv[1] && !!/webpack-dev-server$/.exec(process.argv[1]);
}

function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [ROOT].concat(args));
}

function orderByList(list) {
  return function(chunk1, chunk2) {
    const index1 = list.indexOf(chunk1.names[0]);
    const index2 = list.indexOf(chunk2.names[0]);
    if (index2 === -1 || index1 < index2) {
      return -1;
    }
    if (index1 === -1 || index1 > index2) {
      return 1;
    }
    return 0;
  };
}

exports.orderByList = orderByList;
exports.hasProcessFlag = hasProcessFlag;
exports.isWebpackDevServer = isWebpackDevServer;
exports.root = root;
