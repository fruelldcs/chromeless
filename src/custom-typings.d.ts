declare var ENV: string;
declare var IE: number;
declare var HMR: boolean;
declare var TS_VERSION: string;

interface ErrorStackTraceLimit {
  stackTraceLimit: number;
}

interface ErrorConstructor extends ErrorStackTraceLimit { }

interface NodeModule {
  hot: any;
}

// until the npm package delivers a working definition file
interface IConfigureStore {
  (middlewares: Array<any>): any;
}

declare var configureStore: IConfigureStore;


declare namespace jasmine {
    interface Matchers {
        toEqualImmutable(expected: any): void;
    }
}


interface Window {
    resetWorker: () => void;
    settings: any;
}

interface Navigator {
  serviceWorker: any;
}

// to directyly import json files f.e.
// import foo from './example.json'
declare module '*.json' {
  const value: any;
  export default value;
}
