import { NgModule, ApplicationRef, NgZone } from '@angular/core';
import { NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store';

import {
  RootReducer, RootEpic,
  IState, MainBaseModule, Angular2UtilsModule
} from '@dcs/angular2-utils';

import { AppComponent } from './app/app.component';
import { SharedModule } from './app/shared/shared.module';
import { AppModule } from './app/app.module';


console.time('bootstrap angular');


@NgModule({
  bootstrap: [
    AppComponent
  ],
  declarations: [
  ],
  imports: [
    // app
    SharedModule,
    AppModule,
    // vendors
    Angular2UtilsModule,
    NgReduxModule
  ],
  providers: [
  ]
})
export class MainModule extends MainBaseModule {
  constructor(
    appRef: ApplicationRef,
    store: NgRedux<IState>,
    devTools: DevToolsExtension,
    zone: NgZone,
    rootReducer: RootReducer,
    rootEpic: RootEpic
  ) {
    super(appRef, store, devTools, zone, rootReducer, rootEpic);
    let appState: IState;

    if (module.hot && module.hot.data) {
      appState = module.hot.data.appState;
    }
    this.setupStore(appState);
  }

}
