import { Routes } from '@angular/router';


export const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', loadChildren: 'app/home/home.module#HomeModule' },
  { path: '**', redirectTo: '/not-found', pathMatch: 'full' },
  { path: 'not-found', loadChildren: 'app/not-found/not-found.module#NotFoundModule' },
];
