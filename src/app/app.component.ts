import { Component, OnInit, VERSION } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

/**
 * The entry point into the angular app
 *
 * @export
 * @class AppComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'dcs-app',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(private translate: TranslateService) {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.logBuildSetup(event.lang);
    });
  }

  /**
   * Just some initial logging
   *
   * @memberOf AppComponent
   */
  ngOnInit(): void {
    console.timeEnd('bootstrap angular');
    this.logBuildSetup(this.translate.currentLang);
    // window['app'] = this;
  }

  switchLanguage(locale: string) {
    if (this.translate.langs.includes(locale)) {
      this.translate.use(locale);
    } else {
      throw new Error(`No translations exist for language ${locale}!`);
    }
  }

  logBuildSetup(locale: string) {
    console.log(this.translate.instant('BUILD_SETUP', {
      angularVersion: VERSION.full,
      tsVersion: TS_VERSION,
      env: ENV,
      locale: locale
    }));
  }

}
