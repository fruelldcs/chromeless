import { bangGreetingSelector } from './../backend/greetings.selectors';
import { Subject } from 'rxjs/Subject';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';

import { expect } from 'chai';
import { sandbox, SinonSandbox } from 'sinon';
import { TestBed, inject } from '@angular/core/testing';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/testing';

import { Angular2UtilsModule, APP_SETTINGS, RouterActions, IState } from '@dcs/angular2-utils';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let subject: HomeComponent;
  let whoStub: Subject<string>;
  let sinonSandbox: SinonSandbox;

  beforeEach(() => {
    TestBed.resetTestingModule();
    MockNgRedux.reset();
    sinonSandbox = sandbox.create();

    TestBed.configureTestingModule({
      declarations: [],
      imports: [NgReduxTestingModule, Angular2UtilsModule, RouterModule.forRoot([])],
      providers: [{ provide: APP_SETTINGS, useValue: {} }, { provide: APP_BASE_HREF, useValue: '/' }]
    });
  });

  beforeEach(
    inject([RouterActions], (routerActions: RouterActions) => {
      subject = new HomeComponent(MockNgRedux.getInstance(), routerActions);
      whoStub = MockNgRedux.getSelectorStub<IState, string>(bangGreetingSelector);
    })
  );

  afterEach(() => {
    sinonSandbox.restore();
  });

  it('stores the correct value inside the who property', () => {
    subject.ngOnInit();
    expect(subject.who).to.equal(undefined);

    whoStub.next('Welt');
    expect(subject.who).to.equal('Welt');
  });

  describe('ngOnInit', () => {
    it('dispatches the PING action', () => {
      const dispatchSpy = sinonSandbox.spy(MockNgRedux.mockInstance, 'dispatch');
      subject.ngOnInit();
      expect(dispatchSpy).to.have.been.calledWith({ type: 'PING' });
    });
  });
});
