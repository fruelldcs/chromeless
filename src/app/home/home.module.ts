import { NgModule, Inject } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgRedux } from '@angular-redux/store';
import { TranslateService } from '@ngx-translate/core';
import { RootReducer, IState, DynamicModule, APP_TRANSLATIONS, APP_REDUCERS } from '@dcs/angular2-utils';

import { HomeComponent } from './home.component';
import { SharedModule } from '../shared/shared.module';
import { homeReducer } from './home.reducer';

import * as translationsEn from './locale/en.json';
import * as translationsDe from './locale/de-DE.json';

// Because https://github.com/angular/angular/issues/11402
export function wrapInFunctionBecauseAngularBug11402En() {
  return { name: 'en', translations: translationsEn };
}

export function wrapInFunctionBecauseAngularBug11402De() {
  return { name: 'de-DE', translations: translationsDe };
}

const routes: Routes = [{ path: '', component: HomeComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [HomeComponent],
  providers: [
    { provide: APP_REDUCERS, useValue: { name: 'home', reducer: homeReducer }, multi: true },
    { provide: APP_TRANSLATIONS, useFactory: wrapInFunctionBecauseAngularBug11402En, multi: true },
    { provide: APP_TRANSLATIONS, useFactory: wrapInFunctionBecauseAngularBug11402De, multi: true }
  ]
})
export class HomeModule extends DynamicModule {
  constructor(
    @Inject(APP_TRANSLATIONS) translations: Array<{ name: string; translations: any }>,
    @Inject(APP_REDUCERS) appReducers: any,
    translate: TranslateService,
    rootReducer: RootReducer,
    store: NgRedux<IState>
  ) {
    super(translate, translations, rootReducer, store, appReducers);
  }
}
