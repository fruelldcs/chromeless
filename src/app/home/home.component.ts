import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { NgRedux } from '@angular-redux/store';
import { IState, RouterActions, ContainerComponent } from '@dcs/angular2-utils';

import { bangGreetingSelector } from '../backend/greetings.selectors';

@Component({
  selector: 'dcs-home',
  templateUrl: './home.component.html',
  styles: [
    `
      h1 { color: darkblue; }
    `
  ]
})
export class HomeComponent extends ContainerComponent implements OnInit {
  who$: Observable<string>;
  who: string;

  errors: any = {
    required: false,
    validateTheAnswer: false
  };

  fooControl: FormControl = new FormControl('', []);

  constructor(private store: NgRedux<IState>, private routerActions: RouterActions) {
    super();

    this.who$ = this.store.select(bangGreetingSelector);
  }

  ngOnInit() {
    this.valueFromObservable(this.who$, 'who');
    this.store.dispatch({ type: 'PING' });
  }

  goAway() {
    this.store.dispatch(this.routerActions.routeChange('/foobar/baz'));
  }

  goToGoogle() {
    this.store.dispatch(this.routerActions.routeChange('https://www.google.de/#q=DCS+F%C3%BCrth'));
  }
}
