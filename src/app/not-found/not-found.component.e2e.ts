import { expect } from 'chai';

describe('not found page', function() {
  it('renders the page when hitting a ', function() {
    browser.url('http://localhost:3000/not-existing-page');
    browser.waitForVisible('h1');

    // browser.saveScreenshot('foo.png');
    expect(browser.getUrl()).to.equal('http://localhost:3000/not-found');
    expect(browser.getTitle()).to.include('DCS Angular Starter Project');
    expect(browser.getText('h1')).to.equal('Seite nicht gefunden');
  });
});
