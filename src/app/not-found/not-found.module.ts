import { NgModule, Inject } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgRedux } from '@angular-redux/store';
import { APP_REDUCERS, RootReducer, IState, DynamicModule, APP_TRANSLATIONS } from '@dcs/angular2-utils';

import { SharedModule } from '../shared/shared.module';
import { NotFoundComponent } from './not-found.component';
import { greetingReducer } from '../backend/greetings.reducer';

import * as translationsEn from './locale/en.json';
import * as translationsDe from './locale/de-DE.json';

// Because https://github.com/angular/angular/issues/11402
export function wrapInFunctionBecauseAngularBug11402En() {
  return { name: 'en', translations: translationsEn };
}

export function wrapInFunctionBecauseAngularBug11402De() {
  return { name: 'de-DE', translations: translationsDe };
}

const routes: Routes = [{ path: '', component: NotFoundComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [NotFoundComponent],
  providers: [
    { provide: APP_REDUCERS, useValue: { name: 'nf', reducer: greetingReducer }, multi: true },
    { provide: APP_TRANSLATIONS, useValue: wrapInFunctionBecauseAngularBug11402En, multi: true },
    { provide: APP_TRANSLATIONS, useFactory: wrapInFunctionBecauseAngularBug11402De, multi: true }
  ]
})
export class NotFoundModule extends DynamicModule {
  constructor(
    @Inject(APP_TRANSLATIONS) translations: Array<{ name: string; translations: any }>,
    @Inject(APP_REDUCERS) appReducers: any,
    translate: TranslateService,
    rootReducer: RootReducer,
    store: NgRedux<IState>
  ) {
    super(translate, translations, rootReducer, store, appReducers);
  }
}
