import { createSelector, IState } from '@dcs/angular2-utils';

export function greetingSelector(state: IState): string {
  return state.getIn(['greeting', 'who']);
}

export const bangGreetingSelector = createSelector([greetingSelector], (greeting: string) => `${greeting} !!!`);
