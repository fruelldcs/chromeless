import { fromJS } from 'immutable';

import { IAction, IState } from '@dcs/angular2-utils';


// dummy reducer to show the concept

const initialState: IState = fromJS({
  who: 'Unknown'
});

export function greetingReducer(state: any = initialState, action: IAction): IState {
  switch (action.type) {

    case 'PING':
      return state.set('who', 'World');

    case 'PONG':
      return state.set('who', 'DCS Fürth');
  }

  return state;
}
