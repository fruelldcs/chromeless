import { ErrorHandler } from '@angular/core';


export class AppErrorHandler implements ErrorHandler {

  handleError(error: Error) {
    // do something with the exception
    console.error(error);
  }
}
